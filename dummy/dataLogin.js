export const dataLogin = {
    "user": {
        "role": "user",
        "isEmailVerified": true,
        "email": "vega.yobel.2@gmail.com",
        "name": "Vega Yobel",
        "id": "624834cf98b5e97fae165b00"
    },
    "tokens": {
        "access": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQ4MzRjZjk4YjVlOTdmYWUxNjViMDAiLCJpYXQiOjE2NTA2MjQ4MDgsImV4cCI6MTY1MDYyNjYwOCwidHlwZSI6ImFjY2VzcyJ9.SYwCZKC8fl4m4lu7BN-QxVoF5FgNzvuJlZFx9mBYuQA",
            "expires": "2022-04-22T11:23:28.729Z"
        },
        "refresh": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQ4MzRjZjk4YjVlOTdmYWUxNjViMDAiLCJpYXQiOjE2NTA2MjQ4MDgsImV4cCI6MTY1MzIxNjgwOCwidHlwZSI6InJlZnJlc2gifQ.MrL8xA1mYvxZpc7pymRJvL-aXJjxtr2QylAP6kqhrwo",
            "expires": "2022-05-22T10:53:28.729Z"
        }
    }
};