
describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('login scenario', async () => {
    await expect(element(by.id('login_view'))).toBeVisible();
    await expect(element(by.id('email_input'))).toBeVisible();
    await element(by.id('email_input')).replaceText('vega.yobel.2@gmail.com');
    await expect(element(by.id('password_input'))).toBeVisible();
    await element(by.id('password_input')).replaceText('vegayobel2');
    await element(by.id('login_button')).tap();
    await expect(element(by.id('home_view'))).toBeVisible();
    await element(by.id('logout_button')).tap();
  });
  
  it('register scenario', async () => {
    await expect(element(by.id('login_view'))).toBeVisible();
    await element(by.id('register_nav_button')).tap();
    await expect(element(by.id('name_input'))).toBeVisible();
    await element(by.id('name_input')).replaceText('tessting');
    await expect(element(by.id('email_input'))).toBeVisible();
    await element(by.id('email_input')).replaceText('lololop3@email.com');
    await expect(element(by.id('password_input'))).toBeVisible();
    await element(by.id('password_input')).replaceText('testing1234');
    await element(by.id('register_button')).tap();
    await expect(element(by.id('success_register_view'))).toBeVisible();
  });

  it('register then scenario', async () => {
    await expect(element(by.id('login_view'))).toBeVisible();
    await element(by.id('register_nav_button')).tap();
    await expect(element(by.id('name_input'))).toBeVisible();
    await element(by.id('name_input')).replaceText('tessting');
    await expect(element(by.id('email_input'))).toBeVisible();
    await element(by.id('email_input')).replaceText('lololop4@email.com');
    await expect(element(by.id('password_input'))).toBeVisible();
    await element(by.id('password_input')).replaceText('testing1234');
    await element(by.id('register_button')).tap();
    await expect(element(by.id('success_register_view'))).toBeVisible();
    await element(by.id('login_nav_button')).tap();
    await expect(element(by.id('login_view'))).toBeVisible();
    await expect(element(by.id('email_input'))).toBeVisible();
    await element(by.id('email_input')).replaceText('lololop4@email.com');
    await expect(element(by.id('password_input'))).toBeVisible();
    await element(by.id('password_input')).replaceText('testing1234');
    await element(by.id('login_button')).tap();
    await expect(element(by.id('home_view'))).toBeVisible();
  });
});
