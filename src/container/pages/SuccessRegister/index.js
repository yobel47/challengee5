import {
  Text,
  View,
  ScrollView,
  StatusBar,
} from 'react-native';
import React from 'react';
import { Button } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import { useSelector } from 'react-redux';
import styles from './styles';
import { primaryColor } from '../../utils/colors';
import { DividerLine, HeaderImage, Title } from '../../components';
import { LoadingImage, SuccessImage } from '../../assets';

function SuccessRegister({ navigation }) {
  const isLoading = useSelector((state) => state.app.isLoading);

  return (
    <ScrollView testID='success_register_view' keyboardShouldPersistTaps="handled" style={styles.container}>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LottieView
            source={LoadingImage}
            style={styles.loadingImage}
            autoPlay
            loop
          />
        </View>
      ) : (
        <View style={styles.containerContent}>
          <HeaderImage type="successRegister" />
          <DividerLine borderStyle={styles.dividerLine} />
          <View style={styles.formContainer}>
            <Title text="Registration Completed" />
            <View style={styles.successContainer}>
              <LottieView
                source={SuccessImage}
                loop={false}
                autoPlay
                style={styles.success}
              />
            </View>
            <Text style={styles.footerText}>
              Please check your email address for validation!
            </Text>
            <Button
              testID='login_nav_button'
              mode="contained"
              style={styles.loginButton}
              onPress={() => {
                navigation.navigate('MainApp');
              }}
            >
              Back to Login
            </Button>
          </View>
        </View>
      )}
    </ScrollView>
  );
}

export default SuccessRegister;
