import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import SuccessRegister from './SuccessRegister';
import Home from './Home';
import Detail from './Detail';
import NoInternet from './NoInternet';
import PdfPage from './PdfPage';
import VideoPage from './VideoPage';

export {
  Splash, Login, Register, SuccessRegister, Home, Detail, NoInternet, PdfPage, VideoPage
};
