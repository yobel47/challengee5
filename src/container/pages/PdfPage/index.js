import { View, Text, Button } from 'react-native'
import React from 'react'
import Pdf from 'react-native-pdf';

const PdfPage = ({navigation}) => {
  return (
    <View style={{
        alignItems:'center',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white'
    }}>
      <Button title="Open Video" onPress={()=>{navigation.navigate('VideoPage');}}/>
      <Pdf 
        source={require('../../assets/pdf/thereactnativebook-sample.pdf')}
        style={{height:600,width:600, marginTop:20}}
      />
    </View>
  )
}

export default PdfPage