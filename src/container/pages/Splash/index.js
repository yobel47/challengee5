import { Text, View, StatusBar } from 'react-native';
import React, { useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { BookImage } from '../../assets';
import styles from './styles';

function Splash({ navigation }) {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 1200);
  });

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <View style={styles.logoContainer}>
        <LottieView
          source={BookImage}
          style={styles.logo}
          resizeMode="cover"
          autoPlay={true}
          loop
        />
        <Text style={styles.logoText}>bo͝ok</Text>
      </View>
      <View style={styles.ccContainer}>
        <View style={styles.ccBackground}>
          <Text style={styles.ccText}>bell</Text>
        </View>
      </View>
    </View>
  );
}

export default Splash;
