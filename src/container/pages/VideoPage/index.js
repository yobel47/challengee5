import { View, Text } from 'react-native'
import React from 'react'
import Video from 'react-native-video'

const VideoPage = () => {
  return (
    <View style={{alignItems:'center', flex:1, justifyContent:'center'}}>
      <Text>Video Project</Text>
      <Video
        source={require('../../assets/video/big_buck_bunny.mp4')}
        style={{
            width: 300,
            height: 300,
        }}
        controls={true}
        resizeMode="contain"
      />
    </View>
  )
}

export default VideoPage