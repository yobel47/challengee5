import {
  Text,
  View,
  ScrollView,
  StatusBar,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  TextInput,
  Button,
  HelperText,
  Dialog,
  Portal,
} from 'react-native-paper';
import LottieView from 'lottie-react-native';
import styles from './styles';
import { primaryColor } from '../../utils/colors';
import {
  DividerLine, HeaderImage, TextLink, Title,
} from '../../components';
import { FailedImage, LoadingImage } from '../../assets';
import { checkLogin, setAlert, setLoading } from '../../../redux/actions';

function Login({ navigation }) {
  const [passwordVisible, setPasswordVisible] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailCorrect, setEmailCorrect] = useState(false);
  const [passwordCorrect, setPasswordCorrect] = useState(false);
  const [inputEmailColor, setInputEmailColor] = useState('');
  const [inputPasswordColor, setInputPasswordColor] = useState('');

  const dispatch = useDispatch();

  const isLoading = useSelector((state) => state.app.isLoading);
  const showAlert = useSelector((state) => state.app.showAlert);
  const errorMessage = useSelector((state) => state.app.alertMessage);

  const login = (payload) => dispatch(checkLogin(payload));
  const actionSetLoading = (loading) => dispatch(setLoading(loading));
  const showAlertDialog = (visible) => dispatch(setAlert(visible));
  const hideDialog = () => showAlertDialog(false);

  useEffect(() => {
    actionSetLoading(true);

    setTimeout(() => {
      actionSetLoading(false);
    }, 1200);
  }, []);

  const emailValidate = (emailInput) => {
    setEmail(emailInput);
    const reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(emailInput) === false) {
      setEmailCorrect(true);
      setInputEmailColor('red');
    } else {
      setEmailCorrect(false);
      setInputEmailColor(primaryColor);
    }
  };

  const passwordValidate = (passwordInput) => {
    setPassword(passwordInput);
    const reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
    if (passwordInput.length < 8) {
      setPasswordCorrect(true);
      setInputPasswordColor('red');
    } else if (reg.test(passwordInput) === false) {
      setPasswordCorrect(true);
      setInputPasswordColor('red');
    } else {
      setPasswordCorrect(false);
      setInputPasswordColor(primaryColor);
    }
  };

  const checkLoginInput = (payload) => {
    if (!emailCorrect) {
      if (!passwordCorrect) {
        if (email === '' && password === '') {
          setEmailCorrect(true);
          setInputEmailColor('red');
          setPasswordCorrect(true);
          setInputPasswordColor('red');
        } else {
          login(payload);
        }
      } else {
        setPasswordCorrect(true);
        setInputPasswordColor('red');
      }
    } else {
      setEmailCorrect(true);
      setInputEmailColor('red');
    }
  };

  return (
    <ScrollView key="login" testID='login_view' keyboardShouldPersistTaps="handled" style={styles.container}>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LottieView
            source={LoadingImage}
            style={styles.loadingImage}
            autoPlay
            loop
          />
        </View>
      ) : (
        <View style={styles.containerContent}>
          <HeaderImage type="login" />
          <DividerLine borderStyle={styles.dividerLine} />
          <View style={styles.formContainer}>
            <Title text="Login" />
            <TextInput
              testID="email_input"
              placeholder="Email"
              mode="outlined"
              label="Email"
              style={styles.textInput}
              value={email}
              onChangeText={(text) => emailValidate(text)}
              outlineColor={inputEmailColor}
              activeOutlineColor={inputEmailColor}
            />
            <HelperText
              type="error"
              visible={emailCorrect}
              style={{ marginBottom: -10 }}
            >
              Email address is invalid!
            </HelperText>

            <TextInput
              testID="password_input"
              placeholder="Password"
              mode="outlined"
              label="Password"
              style={styles.textInput}
              value={password}
              onChangeText={(text) => passwordValidate(text)}
              secureTextEntry={passwordVisible}
              outlineColor={inputPasswordColor}
              activeOutlineColor={inputPasswordColor}
              right={(
                <TextInput.Icon
                  name={passwordVisible ? 'eye' : 'eye-off'}
                  onPress={() => setPasswordVisible(!passwordVisible)}
                />
              )}
            />
            <HelperText
              type="error"
              visible={passwordCorrect}
              style={{ marginBottom: -10 }}
            >
              Password is invalid!
            </HelperText>

            <Button
              testID="login_button"
              mode="contained"
              style={styles.loginButton}
              onPress={() => checkLoginInput({ email, password })}
            >
              Login
            </Button>
            <Text style={styles.footerText}>Don&apos;t have an account?</Text>
            <TextLink
              navigation={navigation}
              direction="Register"
              text="Register"
            />
          </View>
        </View>
      )}

      <Portal>
        <Dialog visible={showAlert} dismissable={false}>
          <Dialog.Title style={styles.dialogTitle}>
            <Text>Failed</Text>
          </Dialog.Title>
          <Dialog.Content style={styles.dialogContentContainer}>
            <LottieView
              source={FailedImage}
              loop={false}
              autoPlay
              style={styles.dialogLogo}
              resizeMode="cover"
            />
            <Text style={styles.dialogContentText}>
              {errorMessage}
              !
            </Text>
            <Button
              mode="contained"
              style={styles.loginButton}
              onPress={() => hideDialog()}
            >
              Close
            </Button>
          </Dialog.Content>

          <View style={styles.closeAlertContainer}>
            <View style={styles.closeButton}>
              <Button
                icon="close"
                mode="contained"
                style={{
                  marginLeft: 16,
                }}
                onPress={() => hideDialog()}
              />
            </View>
          </View>
        </Dialog>
      </Portal>
    </ScrollView>
  );
}

export default Login;
