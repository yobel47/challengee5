import { StyleSheet, Dimensions } from 'react-native';
import { primaryColor } from '../../utils/colors';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    marginTop: 62,
  },
  loadingContainer: {
    width: window.width,
    height: window.height * 0.94,
    justifyContent: 'center',
  },
  loadingImage: {
    width: window.width,
  },
  headerImage: {
    marginHorizontal: 24,
    marginBottom: 12,
    flexDirection: 'row',
  },
  headerTextContainer: {
    flex: 1,
    marginRight: 24,
    marginVertical: 12,
    justifyContent: 'flex-start',
  },
  headerTextTitle: {
    color: 'black',
    fontFamily: 'Poppins-Bold',
    fontSize: 24,
  },
  headerTextBy: {
    color: 'black',
    fontFamily: 'Poppins-Medium',
    fontSize: 20,
    marginTop: -6,
  },
  headerText: {
    color: 'black',
    fontFamily: 'Poppins-Light',
    fontSize: 18,
    marginTop: -6,
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 18,
  },
  infoContentContainer: {
    flex: 1,
    alignItems: 'center',
    textAlignVertical: 'center',
    paddingVertical: 12,
    borderRightWidth: 3,
    borderColor: primaryColor,
  },
  infoTextTitle: {
    color: 'black',
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    marginTop: -6,
  },
  infoText: {
    color: 'black',
    fontFamily: 'Poppins-Light',
    fontSize: 24,
    marginTop: -6,
  },
  buyButton: {
    flex: 2,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  overviewContainer: {
    marginHorizontal: 24,
    marginBottom: 24,
  },
  backButton: {
    position: 'absolute',
    backgroundColor: primaryColor,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    overflow: 'hidden',
    marginTop: 10,
    marginLeft: 10,
  },
  rightButtonContainer: {
    position: 'absolute',
    flexDirection: 'row',
    marginTop: 10,
    alignSelf: 'flex-end',
  },
  heartButton: {
    backgroundColor: primaryColor,
    width: 40,
    height: 40,
    borderRadius: 50,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  shareButton: {
    backgroundColor: primaryColor,
    width: 40,
    height: 40,
    borderRadius: 50,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  bookList: {
    backgroundColor: 'red',
  },
});

export default styles;
