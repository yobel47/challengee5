import {
  StyleSheet, View, Dimensions, TouchableOpacity,
} from 'react-native';
import React, { useState, useRef } from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useDispatch } from 'react-redux';
import Poster from '../Poster';
import { setLoading } from '../../../redux/actions';

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: -10,
    backgroundColor: 'rgba(0, 0, 0, 0.92)',
  },
});

function BookCarousel({ navigation, direction, data }) {
  const [activeIndex, setActiveIndex] = useState(0);
  const isCarousel = useRef(null);

  const dispatch = useDispatch();
  const actionSetLoading = (loading) => dispatch(setLoading(loading));
  return (
    <View>
      <Carousel
        data={data}
        ref={isCarousel}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={{ backgroundColor: 'white', alignItems: 'center' }}
            onPress={() => navigation.navigate(
              direction,
              { id: item.id },
              actionSetLoading(true),
            )}
          >
            <Poster
              imgUrl={item.cover_image}
              index={item.id}
              imgSize={{
                width: window.width * 0.4 - 20,
                height: window.width * 0.6,
              }}
            />
          </TouchableOpacity>
        )}
        sliderWidth={window.width}
        itemWidth={190}
        itemHeight={350}
        onSnapToItem={(index) => setActiveIndex(index)}
        autoplay={false}
        loop={false}
      />
      <Pagination
        dotsLength={data?.length}
        activeDotIndex={activeIndex}
        carouselRef={isCarousel}
        dotStyle={styles.dotStyle}
        inactiveDotOpacity={0.7}
        inactiveDotScale={0.6}
        tappableDots
      />
    </View>
  );
}

export default BookCarousel;
