import {
  StyleSheet, Text, View, Pressable, Dimensions,
} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { formatCurrency } from 'react-native-format-currency';
import Poster from '../Poster';
import { primaryColor, thirdColor } from '../../utils/colors';

const window = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 36,
    marginBottom: 24,
    borderRadius: 35,
    overflow: 'hidden',
  },
  textCard: {
    flex: 1.5,
    borderRadius: 30,
    overflow: 'hidden',
    marginVertical: 24,
    marginHorizontal: 24,
    backgroundColor: 'rgba(0,0,0,.8)',
    justifyContent: 'space-between',
  },
  textContainer: {
    flex: 1,
    justifyContent: 'space-between',
    marginHorizontal: 14,
    marginVertical: 18,
  },
  title: {
    fontFamily: 'Poppins-ExtraBold',
    fontSize: 14,
    color: 'white',
  },
  text: {
    fontFamily: 'Poppins-Medium',
    color: 'white',
    fontSize: 12,
  },
  priceContainer: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 35,
    padding: 5,
    paddingHorizontal: 10,
    borderRadius: 50,
    marginTop: 6,
  },
  price: {
    fontFamily: 'Poppins-ExtraBold',
    fontSize: 12,
    color: 'black',
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingTop: 3,
  },
});

function BookCard({ action, data }) {
  const price = () => (data.price ? data.price : 0);
  const [valueFormattedWithSymbol] = formatCurrency({ amount: price(), code: 'IDR' });
  return (
    <Pressable
      onPress={action}
      style={({ pressed }) => [
        {
          backgroundColor: pressed ? primaryColor : thirdColor,
        },
        styles.container,
      ]}
    >
      <View style={{ flexDirection: 'row' }}>
        <Poster
          imgUrl={data.cover_image}
          imgSize={{ width: window.width * 0.3, height: window.width * 0.4 }}
          style={{
            marginVertical: 24,
            marginLeft: 24,
          }}
        />
        <View style={styles.textCard}>
          <View style={[styles.textContainer]}>
            <Text style={styles.title}>{data.title}</Text>
            <Text style={styles.text}>{data.author}</Text>
            <Text style={styles.text}>{data.publisher}</Text>
            <Text style={[styles.text, { fontSize: 22 }]}>
              {data.average_rating}
              {' '}
              <Icon name="star" style={{ fontSize: 22 }} />
            </Text>
            <View style={styles.priceContainer}>
              <Text style={styles.price}>{valueFormattedWithSymbol}</Text>
            </View>
          </View>
        </View>
      </View>
    </Pressable>
  );
}

export default BookCard;
