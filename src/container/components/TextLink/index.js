import {
  StyleSheet, Text, TouchableOpacity,
} from 'react-native';
import React from 'react';
import { primaryColor } from '../../utils/colors';

const styles = StyleSheet.create({
  button: {
    width: 100,
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    fontSize: 16,
    color: primaryColor,
  },
});

function TextLink({ navigation, direction, text }) {
  return (
    <TouchableOpacity
      testID="register_nav_button"
      onPress={() => navigation.navigate(direction)}
      style={styles.button}
    >
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
}

export default TextLink;
