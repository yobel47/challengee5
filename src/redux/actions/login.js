import axios from 'axios';
import { LOGIN_SUCCESS, LOGIN_FAILED } from '../types';
import { setAlert, setLoading, setLogin } from './global';

export const successLogin = (payload, name) => ({
  type: LOGIN_SUCCESS,
  payload: payload,
  name: name
});

export const failedLogin = () => ({
  type: LOGIN_FAILED,
});

export const checkLogin = (payload) => async (dispatch) => {
  dispatch(setLoading(true));
  await axios
    .post('http://code.aldipee.com/api/v1/auth/login', payload)
    .then((res) => {
      dispatch(successLogin(res.data, res.data.user.name));
      dispatch(setLogin(true));
      return res.data;
    })
    .catch((err) => {
      dispatch(failedLogin());
      dispatch(setLogin(false));
      dispatch(setLoading(false));
      dispatch(setAlert(true, err.response.data.message));
    });
};
