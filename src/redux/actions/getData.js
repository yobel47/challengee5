import axios from 'axios';
import { GET_SUCCESS, GET_FAILED } from '../types';
import { setLoading, setLogin, setAlert } from './global';

export const successGet = (payload, recommendedData) => ({
  type: GET_SUCCESS,
  popular: payload,
  recommend: recommendedData,
});

export const failedGet = () => ({
  type: GET_FAILED,
});

export const getData = (token) => async (dispatch) => {
  dispatch(setLoading(true));
  await axios
    .get('http://code.aldipee.com/api/v1/books', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      const recommendedData = res.data.results.slice()
      .sort((a, b) => b.average_rating - a.average_rating)
      .slice(0, 6);
      dispatch(successGet(res.data.results, recommendedData));
      dispatch(setLoading(false));
    })
    .catch((err) => {
      if (err.response.data.message) {
        dispatch(failedGet());
        dispatch(setLogin(false));
        dispatch(setLoading(false));
        dispatch(setAlert(true, err.response.data.message));
      }
    });
};
