import axios from 'axios';
import { GET_DETAIL_SUCCESS, GET_DETAIL_FAILED } from '../types';
import { setLoading, setLogin, setAlert } from './global';

export const successGetDetail = (payload) => ({
  type: GET_DETAIL_SUCCESS,
  payload,
});

export const failedGetDetail = () => ({
  type: GET_DETAIL_FAILED,
});

export const getDataDetail = (id, token) => async (dispatch) => {
  dispatch(setLoading(true));
  await axios
    .get(`http://code.aldipee.com/api/v1/books/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      dispatch(successGetDetail(res.data));
      dispatch(setLoading(false));
    })
    .catch((err) => {
      if (err.response.data.message) {
        dispatch(failedGetDetail());
        dispatch(setLogin(false));
        dispatch(setLoading(false));
        dispatch(setAlert(true, err.response.data.message));
      }
    });
};
