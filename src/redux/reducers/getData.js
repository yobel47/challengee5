import { GET_SUCCESS, GET_FAILED } from '../types';

const initialState = {
  bookData: [],
  recommendBookData: [],
};

export const DataReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case GET_SUCCESS:
      return {
        ...state,
        bookData: action.popular,
        recommendBookData: action.recommend,
      };
    case GET_FAILED:
      return {
        ...state,
      };

    default:
      return state;
  }
};

export default DataReducer;
