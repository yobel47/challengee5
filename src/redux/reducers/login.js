import { LOGIN_SUCCESS, LOGIN_FAILED } from '../types';

export const initialState = {
  userData: [],
  name: '',
};

const LoginReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        userData: action.payload,
        name: action.name
      };
    case LOGIN_FAILED:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default LoginReducer;
