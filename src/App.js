import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import { LogBox } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Router from './container/router';
import { primaryColor, secondaryColor } from './container/utils/colors';
import { Store, Persistor } from './redux/store';

const theme = {
  ...DefaultTheme,
  roundness: 10,
  colors: {
    ...DefaultTheme.colors,
    primary: primaryColor,
    accent: secondaryColor,
  },
  fonts: {
    regular: {
      fontFamily: 'Poppins-Medium',
    },
  },
};

export default function App() {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <PaperProvider theme={theme}>
          <NavigationContainer>
            <Router />
          </NavigationContainer>
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
}

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
  "ViewPropTypes will be removed from React Native. Migrate to ViewPropTypes exported from 'deprecated-react-native-prop-types",
  'Require cycle: node_modules\react-native-alert-notificationsrcindex.tsx -> node_modules\react-native-alert-notificationsrccontainersindex.ts -> node_modules\react-native-alert-notificationsrccontainersRoot.tsx -> node_modules\react-native-alert-notificationsrcindex.tsx',
]);
