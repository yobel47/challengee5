import {
    shallow, configure, mount, dive
  } from 'enzyme';
  import React from 'react';
  import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
  import { Provider } from 'react-redux';
  import configureStore from 'redux-mock-store';
  import Login from '../src/container/pages/Login';
  import GlobalReducer from '../src/redux/reducers/global';
  import LoginReducer from '../src/redux/reducers/login';
  import RegisterReducer from '../src/redux/reducers/register';
  import GetData from '../src/redux/reducers/getData';
  import GetDetailData from '../src/redux/reducers/getDetailData';
  import Register from '../src/container/pages/Register';
  
  configure({ adapter: new Adapter(), disableLifecycleMethods: true });
  
  describe('Login Screen test', () => {
    const initialState = {
      app: GlobalReducer,
      login: LoginReducer,
      register: RegisterReducer,
      data: GetData,
      detailData: GetDetailData,
    };
  
    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
        <Provider store={store}>
            <Login />
        </Provider>
    );

    describe('Check component', () => {
        it('should have an email field', () => {
          expect(wrapper.find('[testID="email_input"]')).toBeTruthy();
        });
        it('should have a password field', () => {
          expect(wrapper.find('[testID="password_input"]')).toBeTruthy();
        });
        it('should have a login button', () => {
          expect(wrapper.find('[testID="button_login"]')).toBeTruthy();
        });
      });
    
    describe('Event test',  () => {
        wrapper.find('[testID="email_input"]').at(5).simulate('change');
        wrapper.find('[testID="password_input"]').at(0).simulate('change');
        wrapper.find('[testID="login_button"]').at(0).simulate('click');
    });
  });
  
  describe('Register Screen test', () => {
    const initialState = {
      app: GlobalReducer,
      login: LoginReducer,
      register: RegisterReducer,
      data: GetData,
      detailData: GetDetailData,
    };
  
    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
        <Register />
      </Provider>,
    );
  
    describe('Check component', () => {
        it('should have an name field', () => {
          expect(wrapper.find('[testID="name_input"]')).toBeTruthy();
        });
        it('should have an email field', () => {
          expect(wrapper.find('[testID="email_input"]')).toBeTruthy();
        });
        it('should have a password field', () => {
          expect(wrapper.find('[testID="password_input"]')).toBeTruthy();
        });
        it('should have a register button', () => {
          expect(wrapper.find('[testID="register_button"]')).toBeTruthy();
        });
      });
    
    describe('Event test',  () => {
        wrapper.find('[testID="name_input"]').at(0).simulate('change');
        wrapper.find('[testID="email_input"]').at(0).simulate('change');
        wrapper.find('[testID="password_input"]').at(0).simulate('change');
        wrapper.find('[testID="register_button"]').at(0).simulate('click');
    });
  });
