import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {dataLogin} from '../dummy/dataLogin';
import {dataRegister} from '../dummy/dataRegister';
import {dataBook} from '../dummy/dataBook';

describe('login API', () => {
    const checkLogin = async (payload) => {
        try {
          return await axios.post('http://code.aldipee.com/api/v1/auth/login', { payload });
        } catch (e) {
          return [];
        }
    };

    const payload = {
        email: 'vega.yobel.2@gmail.com',
        password: 'vegayobel2',
    }

    it('Login success', async () => {
        let mock = new MockAdapter(axios);
        mock.onPost('http://code.aldipee.com/api/v1/auth/login').reply(200, dataLogin);
        const result = await checkLogin(payload);
        expect(result.data).toEqual(dataLogin);
        expect(result.status).toEqual(200);
    });

    it('Login failed', async () => {
        let mock = new MockAdapter(axios);
        mock.onPost('http://code.aldipee.com/api/v1/auth/login').reply(400, []);
        const result = await checkLogin(payload);
        expect(result).toEqual([]);
    });
});

describe('register API', () => {
    const checkRegsiter = async (payload) => {
        try {
          return await axios.post('http://code.aldipee.com/api/v1/auth/register', { payload });
        } catch (e) {
          return [];
        }
    };

    const payload = {
        email: 'beeeep@email.com',
        password: 'beeeeeep123',
        name: 'Beep'
    }

    it('Register success', async () => {
        let mock = new MockAdapter(axios);
        mock.onPost('http://code.aldipee.com/api/v1/auth/register').reply(200, dataRegister);
        const result = await checkRegsiter(payload);
        expect(result.data).toEqual(dataRegister);
    });

    it('Register failed', async () => {
        let mock = new MockAdapter(axios);
        mock.onPost('http://code.aldipee.com/api/v1/auth/register').reply(400, []);
        const result = await checkRegsiter(payload);
        expect(result).toEqual([]);
    });
});

describe('Book API', () => {
    const getBook = async (token) => {
        try {
            return await axios.get('http://code.aldipee.com/api/v1/books', {
                headers: {
                  Authorization: `Bearer ${token}`
                },
              });
        } catch (e) {
          return [];
        }
    };

    it('Register success', async () => {
        let mock = new MockAdapter(axios);
        mock.onGet('http://code.aldipee.com/api/v1/books').reply(200, dataBook);
        const result = await getBook('token');
        expect(result.data).toEqual(dataBook);
    });

    it('Register failed', async () => {
        let mock = new MockAdapter(axios);
        mock.onGet('http://code.aldipee.com/api/v1/books').reply(400, []);
        const result = await getBook('token');
        expect(result).toEqual([]);
    });
});




