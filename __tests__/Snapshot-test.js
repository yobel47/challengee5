import {
  shallow, configure, mount, dive
} from 'enzyme';
import React from 'react';
import renderer from 'react-test-renderer';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Login from '../src/container/pages/Login';
import GlobalReducer from '../src/redux/reducers/global';
import LoginReducer from '../src/redux/reducers/login';
import RegisterReducer from '../src/redux/reducers/register';
import GetData from '../src/redux/reducers/getData';
import GetDetailData from '../src/redux/reducers/getDetailData';
import Register from '../src/container/pages/Register';
import Home from '../src/container/pages/Home';

configure({ adapter: new Adapter(), disableLifecycleMethods: true });

describe('Login Screen test', () => {
  const initialState = {
    app: GlobalReducer,
    login: LoginReducer,
    register: RegisterReducer,
    data: GetData,
    detailData: GetDetailData,
  };

  const mockStore = configureStore();
  const store = mockStore(initialState);
  const loginWrapper = shallow(
    <Provider store={store}>
      <Login />
    </Provider>
  );

  it('should render correctly', () => {
    renderer.create(loginWrapper)
  });

  it('should renders `Login Screen` module correctly', () => {
    expect(loginWrapper).toMatchSnapshot();
  });

});

describe('Register Screen test', () => {
  const initialState = {
    app: GlobalReducer,
    login: LoginReducer,
    register: RegisterReducer,
    data: GetData,
    detailData: GetDetailData,
  };

  const mockStore = configureStore();
  const store = mockStore(initialState);
  const registerWrapper = shallow(
    <Provider store={store}>
      <Register />
    </Provider>,
  );

  it('should render correctly', () => {
    renderer.create(registerWrapper)
  });

  it('should renders `Register Screen` module correctly', () => {
    expect(registerWrapper).toMatchSnapshot();
  });
});

describe('Home Screen test', () => {
  const initialState = {
    app: GlobalReducer,
    login: LoginReducer,
    register: RegisterReducer,
    data: GetData,
    detailData: GetDetailData,
  };

  const mockStore = configureStore();
  const store = mockStore(initialState);
  const homeWrapper = shallow(
    <Provider store={store}>
      <Home />
    </Provider>,
  );

  it('should render correctly', () => {
    renderer.create(homeWrapper)
  });

  it('should renders `Home Screen` module correctly', () => {
    expect(homeWrapper).toMatchSnapshot();
  });
});
